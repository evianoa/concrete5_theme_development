# README #

How to use the docker lamp base setup

### What is this repository for? ###

* This repository sets up lamp environment consisting of an apache container and a mysql container. It serves as a good base environment for a concrete5 setup.
* Version 1.0

### How do I get set up? ###

1. Fork this repository 
2. Clone the forked repository into your project folder
3. Create a .data folder
4. Update the content of the .env file

### Resources

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Eviano Afiemo eviano@cleverpixels.net